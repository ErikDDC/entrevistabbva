import { LitElement, html, css } from "lit-element";
import './getData';

class Index extends LitElement {

    static get properties() {
        return {
            resultado: {type: Array},
        }
    }

    static get styles() {
        return css `
            table {
                text-align: center;
                width: 100%;
                border-collapse: collapse;
            }
            thead {
                background-color: #ff5733;
            }
        `;
    }

    constructor() {
        super();
        this.resultado = [];
        this.addEventListener('apiData', (datos) => {
            this.resultado = datos.detail.data.results;
            console.log(datos.detail.data.results);
        })
    }

    render() {
        return html `
        <get-data url="https://api.datos.gob.mx/v1/calidadAire" metodo="GET"></get-data>
        ${this.dataTemplete}`;
    }

    get dataTemplete() {
        return html `
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Latitud</th>
                        <th>Longitud</th>
                    </tr>
                </thead>
                <tbody>
                ${this.resultado.map(elemento => html `
                <tr>
                    <td>${elemento.stations["0"].name}</td>
                    <td>${elemento.stations["0"].location.lat}</td>
                    <td>${elemento.stations["0"].location.lon}</td>
                </tr>
            `)}
                </tbody>
            </table>
        </div>
        `;
    }
}

customElements.define('index-principal', Index);